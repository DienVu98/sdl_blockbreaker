#pragma once
#include "Header.h"

class Sprite
{
public:
	SDL_Texture * texture;
	SDL_Rect Block[NumberOfBlock];
	SDL_Rect sourceRect;
	SDL_Rect desRect;
	void ReadFile(const char * path1, const char * path2, int WhichBlock, int x, int y);
};