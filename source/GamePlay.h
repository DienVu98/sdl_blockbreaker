#pragma once
#include "Header.h"

class GamePlay : public Scene
{
	Texture* texture;
	Music music;
	Music musicEnd;
	SDL_Rect gBrick;
	SDL_Rect gBar;
	int count = 0;
	int tempX = 0, tempY = 0;
	char flag = '0';

	int over = 0;

	int currentFrameIndex = 0;
	Sprite sprite;
public:

	void Init() override;
	void Update(unsigned int dt) override;
	void Render() override;
	void Destroy() override;

	void HandleKeyEvent(char key, bool state) override;
	void HandleMouseEvent(int x, int y, int button) override;

	void RenderFrame(int frameIndex);
};

