#include <stdio.h>
#include <SDL.h>
#include "SDL_image.h"
#include "SDL_mixer.h"
#include "Application.h"

#undef main

int main()
{
	Application *app = new Application;
	app->Init();
	
	SCENE_MGR->ChangeScene(new MainMenu());

	app->Run(60);
	delete app;
	return 0;
}