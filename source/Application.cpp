#include"Header.h"

Application::Application() : window(NULL), renderer(NULL)
{
}

void Application::Init()
{
	if (SDL_Init(SDL_INIT_EVERYTHING) < 0)
	{
		printf("Unable to initialize SDL %s\n", SDL_GetError());
	}

	int imgFlags = IMG_INIT_PNG | IMG_INIT_JPG;
	if (!(IMG_Init(imgFlags) & imgFlags))
	{
		printf("SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError());
	}

	window = SDL_CreateWindow("Stdio.vn - SDL", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, WIDTH, HEIGHT,  SDL_WINDOW_SHOWN);
	if (window == NULL)
	{
		printf("Could not create window %s", SDL_GetError());
	}

	

	//gBrick.x = 30;
	//gBrick.y = 30;
	//gBrick.w = 40;
	//gBrick.h = 40;
	//	// set default value for brick
	//gBar.x = 400;
	//gBar.y = 580;
	//gBar.w = 100;
	//gBar.h = 10;
	//gameplay init 

	if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) == -1)
	{
		printf("%s", Mix_GetError());
	}

	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
	if (renderer == NULL)
	{
		SDL_Log("Unable to init renderer. Error: %s", SDL_GetError());
	}
	Shared::renderer = renderer;

	/*music.Load();
		
	texture = new Texture();
	texture->Load("fireball.png");*/
}

//void Application::render()
//{
//
//	SDL_SetRenderDrawColor(renderer, 255, 255, 255, SDL_ALPHA_OPAQUE);
//	SDL_RenderClear(renderer);
//
//	/*SDL_SetRenderDrawColor(renderer, 255, 0, 0, SDL_ALPHA_OPAQUE);
//	SDL_RenderFillRect(renderer, &gBrick);
//*/
////SDL_RenderCopy(renderer, texture, NULL, &gBrick);
//
//	RenderFrame(currentFrameIndex);
//
//	currentFrameIndex = (currentFrameIndex + 1) % 24;
//
//	sprite.ReadFile("Block.txt", "block.png", 9, 200, 100);
//	SDL_RenderCopy(renderer, sprite.texture, &sprite.sourceRect, &sprite.desRect);
//
//	// de danh khai bao class Brick and Bar
//	SDL_SetRenderDrawColor(renderer, 0, 0, 255, SDL_ALPHA_OPAQUE);
//	SDL_RenderFillRect(renderer, &gBar); 
//
//	SDL_RenderPresent(renderer);
//}


void Application::Destroy()
{
	
	SDL_DestroyWindow(window);
	SDL_DestroyRenderer(renderer);
	SDL_Quit();
}

//void Application::update()
//{	

//}

void Application::QueryEvents()
{
	SDL_Event mainEvent;

	while (SDL_PollEvent(&mainEvent))
	{
		switch (mainEvent.type)
		{
			//User - requested quit
			case SDL_QUIT:
			{
				isRunning = false;
				break;
			}
			case SDL_KEYDOWN:
			{
				SCENE_MGR->HandleKeyEvent(mainEvent.key.keysym.sym, true);
				break;
			}
			case SDL_MOUSEBUTTONDOWN:
			{
				mainEvent.button.button == SDL_BUTTON_LEFT;
				SCENE_MGR->HandleMouseEvent(mainEvent.motion.x, mainEvent.motion.y, 1);
				break;
			}
			case SDL_MOUSEMOTION:
			{
				//
				
			}
			default:
			{
				if (!Mix_PlayingMusic())
					//Mix_PlayMusic(music.music, 0);
				break;
			}
		}
	}
}

void Application::Run(int fps)
{	
	unsigned int frameTime = 1000.0f / fps;
	unsigned int startTime = 0, endTime = 0, deltaTime = frameTime;

	while (isRunning)
	{
		startTime = SDL_GetTicks();

		QueryEvents();

		// *****
		SCENE_MGR->Update(frameTime);
		// *****

		SDL_SetRenderDrawColor(renderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
		SDL_RenderClear(renderer);

		// *****
		SCENE_MGR->Render();
		// *****

		SDL_RenderPresent(renderer);

		endTime = SDL_GetTicks();
		deltaTime = endTime - startTime;

		if (deltaTime < frameTime)
		{
			SDL_Delay(frameTime - deltaTime);
			deltaTime = frameTime;
		}
	}
	Destroy();

	//Init();
	//bool isRunning = true;
	//while (isRunning)
	//{
	//	while (SDL_PollEvent(&mainEvent))
	//	{
	//		switch (mainEvent.type)
	//		{

	//			//User - requested quit
	//			case SDL_QUIT:
	//			{
	//				isRunning = false;
	//				break;
	//			}

	//			case SDL_MOUSEMOTION:
	//			{
	//				//printf("Current Position Mouse: (%d, %d)\n", mainEvent.motion.x, mainEvent.motion.y);
	//				gBar.x = mainEvent.motion.x - gBar.w / 2;
	//			}

	//			default:
	//			{
	//				if (!Mix_PlayingMusic())
	//					Mix_PlayMusic(music.music, 0);
	//				break;
	//			}
	//		}
	//	}
	//		update();
	//		render();
	//		

	//		if (over == -1)
	//		{
	//			printf("over");
	//			Mix_PlayMusic(musicEnd.musicEnd, 0);
	//			SDL_Delay(4000);
	//		}

	//		/*SDL_Delay(1000.0 / 60);*/
	//		SDL_Delay(1000.0 / 60);
	//}
	//destroy();
}
//
//void Application::RenderFrame(int frameIndex)
//{
//	SDL_Rect srcRect;
//	srcRect.x = (frameIndex % 6) * 134;
//	srcRect.y = (frameIndex / 6) * 134;
//	srcRect.w = srcRect.h = 134;
//
//	SDL_RenderCopy(renderer, texture->texture, &srcRect, &gBrick);
//}

