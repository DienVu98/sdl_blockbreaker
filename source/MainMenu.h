#include "Header.h"

class MainMenu : public Scene
{
	Texture*	startButton;
	Texture*	exitButton;
	SDL_Point startButtonPosition;
	SDL_Point exitButtonPosition;

	SDL_Rect startBtnDes;
	SDL_Rect exitBtnDes;

public:

	void Init() override;
	void Update(unsigned int dt) override;
	void Render() override;
	void Destroy() override;

	void HandleKeyEvent(char key, bool state) override;
	void HandleMouseEvent(int x, int y, int button) override;
};