#include "Header.h"


bool Texture::Load(const char * path)
{
	texture = IMG_LoadTexture(Shared::renderer, path);

	if (!texture)
	{
		SDL_Log("Unable to load texture: %s. Error: %s", path, IMG_GetError());
		return false;
	}

	SDL_QueryTexture(texture, NULL, NULL, &size.w, &size.h);

	return true;
}