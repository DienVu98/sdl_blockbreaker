#pragma once
#include "Header.h"

class Application
{
private:
	SDL_Window * window = NULL;
	SDL_Renderer* renderer;

	bool isRunning = true;

public:  
	Application();

	void Init();

	void Destroy();
	void QueryEvents();

	void Run(int fps);
};