#include "Header.h"

void Sprite::ReadFile(const char * path1, const char * path2, int WhichBlock, int x, int y)
{
	texture = IMG_LoadTexture(Shared::renderer, path2);

	FILE *FileIn;
	FileIn = fopen(path1, "r");
	
	for (int i = 1; i <= 16; i++)
	{
		fscanf(FileIn, "%d ", &Block[i].x);
		fscanf(FileIn, "%d ", &Block[i].y);
		fscanf(FileIn, "%d ", &Block[i].w);
		fscanf(FileIn, "%d ", &Block[i].h);
	}

	sourceRect = Block[WhichBlock];
	desRect.x = x;
	desRect.y = y;
	desRect.w = sourceRect.w;
	desRect.h = sourceRect.h;

}
	