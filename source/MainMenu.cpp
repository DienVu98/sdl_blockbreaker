#include "Header.h"

void MainMenu::Init()
{
	startButton = new Texture();

	if (startButton->Load("StartButton.png"))
	{
		SDL_Log("Load start button success");
	}
	else
		SDL_Log("Error while load start button");


	startButtonPosition.x = WIDTH / 2 - startButton->size.w / 2;
	startButtonPosition.y = HEIGHT / 2 - startButton->size.h / 2;

	exitButton = new Texture();
	if (exitButton->Load("quit.png"))
	{
		SDL_Log("Load exit button success");
	}
	else
		SDL_Log("Error while load exit button");

	exitButtonPosition.x = WIDTH - exitButton->size.w;
	exitButtonPosition.y = HEIGHT - exitButton->size.h;
}

void MainMenu::Update(unsigned int dt)
{


}

void MainMenu::Render()
{
	
	startBtnDes.h = startButton->size.h / 2;
	startBtnDes.w = startButton->size.w / 2;
	startBtnDes.x = startButtonPosition.x + startButton->size.h / 2;
	startBtnDes.y = startButtonPosition.x + startButton->size.h / 2	;


	SDL_RenderCopy(Shared::renderer, startButton->texture, NULL, &startBtnDes);

	
	exitBtnDes.h = exitButton->size.h / 2;
	exitBtnDes.w = exitButton->size.w / 2;
	exitBtnDes.x = WIDTH - exitBtnDes.w;
	exitBtnDes.y = HEIGHT - exitBtnDes.h;


	SDL_RenderCopy(Shared::renderer, exitButton->texture, NULL, &exitBtnDes);
}

void MainMenu::Destroy()
{
}

void MainMenu::HandleKeyEvent(char key, bool state = true)
{
	SDL_Log("Key: %c", key);
	if (key == 27)
	{
		SDL_Quit();
	}
}

void MainMenu::HandleMouseEvent(int x, int y, int button)
{
	if (x > startBtnDes.x 
		&& y > startBtnDes.y 
		&& x < startBtnDes.x + startBtnDes.w 
		&& y < startBtnDes.y + startBtnDes.h)
	{
		printf("press");
		SCENE_MGR->ChangeScene(new GamePlay());
	}
}
