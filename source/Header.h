#pragma once
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <iostream>

#include "SDL.h"
#include "SDL_image.h"
#include "SDL_mixer.h"

#include "config.h"
#include "Share.h"
#include "TextTure.h"
#include "Sound.h"
#include "Music.h"
#include "Sprite.h"
#include "Scene.h"
#include "MainMenu.h"
#include "GamePlay.h"
#include "SceneManager.h"
#include "Application.h"