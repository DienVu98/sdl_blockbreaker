#include "GamePlay.h"

void GamePlay::Init()
{
	gBrick.x = 30;
	gBrick.y = 30;
	gBrick.w = 40;
	gBrick.h = 40;
		// set default value for brick
	gBar.x = 400;
	gBar.y = 580;
	gBar.w = 100;
	gBar.h = 10;

	music.Load();

	texture = new Texture();
	texture->Load("fireball.png");
}

void GamePlay::Update(unsigned int dt)
{
	if (tempX == 0 && count == 0)
	{
		if (flag == '0')
		{
			gBrick.x += 3;
			gBrick.y += 3;

			if (gBrick.y == 540 && gBrick.x + gBrick.w / 2 > gBar.x && gBrick.x + gBrick.w / 2 < gBar.x + gBar.w / 2)
			{
				tempY = gBrick.y;
				count = 1;
				flag = '1';
			}

			else if (gBrick.y == 540 && gBrick.x + gBrick.w / 2 > gBar.x + gBar.w / 2 && gBrick.x + gBrick.w / 2 < gBar.x + gBar.w)
			{
				tempY = gBrick.y;
				count = 1;
				flag = '0';
			}

			else if (gBrick.y == 600)
			{
				tempY = gBrick.y;
				count = 1;
				over = -1;
			}

			else if (gBrick.x == 759)
			{
				tempX = gBrick.x;
				count = 2;
				flag = '1';
			}
		}

		else if (flag == '1')
		{
			gBrick.x += 3;
			gBrick.y -= 3;
			if (gBrick.y == 0)
			{
				tempY = gBrick.y;
				count = 3;
			}

			else if (gBrick.x == 759)
			{
				tempX = gBrick.x;
				count = 2;
				flag = '0';
			}
		}
	}

	// bat len phai
	else if (tempY == 540 && count == 1)
	{
		if (flag == '0')
		{
			gBrick.x += 3;
			gBrick.y -= 3;
			if (gBrick.x == 759)
			{
				tempX = gBrick.x;
				count = 2;
			}

			else if (gBrick.y == 0)
			{
				tempY = gBrick.y;
				count = 3;
				flag = '1';
			}
		}

		else if (flag == '1')
		{
			gBrick.x -= 3;
			gBrick.y -= 3;
			if (gBrick.x == 0)
			{
				tempX = gBrick.x;
				count = 0;
			}

			else if (gBrick.y == 0)
			{
				tempY = gBrick.y;
				count = 3;
				flag = '0';
			}
		}
	}

	//bat len trai
	else if (tempX == 759 && count == 2)
	{
		if (flag == '0')
		{
			gBrick.x -= 3;
			gBrick.y -= 3;
			if (gBrick.y == 0)
			{
				tempY = gBrick.y;
				count = 3;
			}

			else if (gBrick.x == 0)
			{
				tempX = gBrick.x;
				count = 0;
				flag = '1';
			}
		}

		else if (flag == '1')
		{
			gBrick.x -= 3;
			gBrick.y += 3;

			if (gBrick.y == 540 && gBrick.x + gBrick.w / 2 > gBar.x && gBrick.x + gBrick.w / 2 < gBar.x + gBar.w / 2)
			{
				tempY = gBrick.y;
				count = 1;
				flag = '1';
			}

			else if (gBrick.y == 540 && gBrick.x + gBrick.w / 2 > gBar.x + gBar.w / 2 && gBrick.x + gBrick.w / 2 < gBar.x + gBar.w)
			{
				tempY = gBrick.y;
				count = 1;
				flag = '0';
			}

			else if (gBrick.y == 600)
			{
				tempY = gBrick.y;
				count = 1;
				over = -1;
			}

			else if (gBrick.x == 0)
			{
				tempX = gBrick.x;
				count = 0;
				flag = '0';
			}
		}
	}

	//bat xuong trai
	else if (tempY == 0 && count == 3)
	{
		if (flag == '0')
		{
			gBrick.x -= 3;
			gBrick.y += 3;

			if (gBrick.y == 540 && gBrick.x + gBrick.w / 2 > gBar.x && gBrick.x + gBrick.w / 2 < gBar.x + gBar.w / 2)
			{
				tempY = gBrick.y;
				count = 1;
				flag = '1';
			}

			else if (gBrick.y == 540 && gBrick.x + gBrick.w / 2 > gBar.x + gBar.w / 2 && gBrick.x + gBrick.w / 2 < gBar.x + gBar.w)
			{
				tempY = gBrick.y;
				count = 1;
				flag = '0';
			}

			else if (gBrick.x == 0)
			{
				tempX = gBrick.x;
				count = 0;
			}

			else if (gBrick.y == 600)
			{
				tempY = gBrick.y;
				count = 1;
				flag = '1';
				over = -1;
			}
		}

		else if (flag == '1')
		{
			gBrick.x += 3;
			gBrick.y += 3;

			if (gBrick.y == 540 && gBrick.x + gBrick.w / 2 > gBar.x && gBrick.x + gBrick.w / 2 < gBar.x + gBar.w / 2)
			{
				tempY = gBrick.y;
				count = 1;
				flag = '1';
			}

			else if (gBrick.y == 540 && gBrick.x + gBrick.w / 2 > gBar.x + gBar.w / 2 && gBrick.x + gBrick.w / 2 < gBar.x + gBar.w)
			{
				tempY = gBrick.y;
				count = 1;
				flag = '0';
			}

			else if (gBrick.x == 759)
			{
				tempX = gBrick.x;
				count = 2;
			}

			else if (gBrick.y == 600)
			{
				tempY = gBrick.y;
				count = 1;
				flag = '0';
				over = -1;
			}
		}
	}
}

void GamePlay::Render()
{
	
	SDL_SetRenderDrawColor(Shared::renderer, 255, 255, 255, SDL_ALPHA_OPAQUE);
	SDL_RenderClear(Shared::renderer);

	RenderFrame(currentFrameIndex);

	currentFrameIndex = (currentFrameIndex + 1) % 24;

	sprite.ReadFile("Block.txt", "block.png", 9, 200, 100);
	SDL_RenderCopy(Shared::renderer, sprite.texture, &sprite.sourceRect, &sprite.desRect);

	SDL_SetRenderDrawColor(Shared::renderer, 0, 0, 255, SDL_ALPHA_OPAQUE);
	SDL_RenderFillRect(Shared::renderer, &gBar);

	SDL_RenderPresent(Shared::renderer);
}

void GamePlay::Destroy()
{
	Mix_CloseAudio();
}

void GamePlay::HandleKeyEvent(char key, bool state)
{
}

void GamePlay::HandleMouseEvent(int x, int y, int button)
{
	gBar.x = x - gBar.w / 2;
}

void GamePlay::RenderFrame(int frameIndex)
{
	SDL_Rect srcRect;
	srcRect.x = (frameIndex % 6) * 134;
	srcRect.y = (frameIndex / 6) * 134;
	srcRect.w = srcRect.h = 134;

	SDL_RenderCopy(Shared::renderer, texture->texture, &srcRect, &gBrick);
}
