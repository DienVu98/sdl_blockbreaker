class Scene {
public:
	virtual void Init() = 0;
	virtual void Update(unsigned int dt) = 0;
	virtual void Render() = 0;
	virtual void Destroy() = 0;

	virtual void HandleKeyEvent(char key, bool state) = 0;
	virtual void HandleMouseEvent(int x, int y, int button) = 0;
};